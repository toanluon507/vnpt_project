
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Spotify - Web Player: Music for everyone</title>
    <meta name="description" content="Spotify clone made using html,css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="./styles.css" />
    <link rel="icon" type="image/x-icon" href="./assets/logo.png" />

    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;700&display=swap"
      rel="stylesheet"
    />
  </head>
  <body>
    <div class="container">
      <div class="sidebar">
        <div class="top-icons">
          <div class="link-flex">
            <img src="./assets/home.png" alt="Home" />
            <a href="#">Home</a>
          </div>
          <div class="link-flex">
            <img src="./assets/search.png" alt="Search" />
            <a href="#">Search</a>
          </div>
        </div>
        <div class="library">
          <div class="flex-top">
            <div class="link-flex">
              <img src="./assets/playlist.png" alt="Library" />
              <p>Your Library</p>
            </div>
            <div class="link-flex">
              <img src="./assets/plus.png" />
              <img src="./assets/boxes.png" />
            </div>
          </div>
          <div class="link-flex" style="gap: 0.5rem">
            <div class="chip">Playlists</div>
            <div class="chip">Artists</div>
            <div class="chip">Podcasts & Shows</div>
          </div>
          <div class="flex-top">
            <img src="./assets/search.png" />
            <p>Recents</p>
          </div>
          <div class="albums">
            <div class="link-flex album-flex">
              <img src="./assets/mix1.jpeg" class="album-cover" />
              <div class="col-flex">
                <p class="album-name">Workout</p>
                <p>Playlist &#x2022; Spotify</p>
              </div>
            </div>
            <div class="link-flex album-flex">
              <img src="./assets/mix1.jpeg" class="album-cover" />
              <div class="col-flex">
                <p class="album-name">Workout</p>
                <p>Playlist &#x2022; Spotify</p>
              </div>
            </div>
           
            
          </div>
          <!-- Put your playlists here -->
        </div>
      </div>
      <div class="main-content">
        <div class="navbar">
          <div class="link-flex">
            <img src="./assets/back_navigation.png" />
            <img src="./assets/forward_navigation.png" />
          </div>
          <!-- Navbar content -->
          <div class="link-flex" style="gap: 0; column-gap: 0.5rem">
            <div class="chip">Upgrade</div>
            <div class="chip">Install App</div>
            <div class="chip">Premium</div>
            <div class="chip">Sign up</div>
            <div class="chip">Log in</div>
          </div>
        </div>
       <div class="spotify-playlists">
        <h2>Spotify Playlists</h2>

        <div class="list">
          <div class="item">
            <img src="https://i.scdn.co/image/ab67616d0000b2733b5e11ca1b063583df9492db" />
            <div class="play">
              <span class="fa fa-play"></span>
            </div>
            <h4>Today's Top Hits</h4>
            <p>Rema & Selena Gomez are on top of the...</p>
          </div>

          <div class="item">
            <img src="https://i.scdn.co/image/ab67616d0000b2733b5e11ca1b063583df9492db" />
            <div class="play">
              <span class="fa fa-play"></span>
            </div>
            <h4>RapCaviar</h4>
            <p>New Music from Lil Baby, Juice WRLD an...</p>
          </div>

          <div class="item">
            <img src="https://i.scdn.co/image/ab67616d0000b2733b5e11ca1b063583df9492db" />
            <div class="play">
              <span class="fa fa-play"></span>
            </div>
            <h4>All out 2010s</h4>
            <p>The biggest spmgs pf tje 2010s. Cover:...</p>
          </div>

          <div class="item">
            <img src="https://i.scdn.co/image/ab67616d0000b2733b5e11ca1b063583df9492db" />
            <div class="play">
              <span class="fa fa-play"></span>
            </div>
            <h4>Rock Classics</h4>
            <p>Rock Legends & epic songs that continue t...</p>
          </div>

          <div class="item">
            <img src="https://i.scdn.co/image/ab67616d0000b2733b5e11ca1b063583df9492db" />
            <div class="play">
              <span class="fa fa-play"></span>
            </div>
            <h4>Chill Hits</h4>
            <p>Kick back to the best new and recent chill...</p>
          </div>

          <div class="item">
            <img src="https://i.scdn.co/image/ab67616d0000b2733b5e11ca1b063583df9492db" />
            <div class="play">
              <span class="fa fa-play"></span>
            </div>
            <h4>Viva Latino</h4>
            <p>Today's top Latin hits elevando nuestra...</p>
          </div>

          <div class="item">
            <img src="https://i.scdn.co/image/ab67616d0000b2733b5e11ca1b063583df9492db" />
            <div class="play">
              <span class="fa fa-play"></span>
            </div>
            <h4>Mega Hit Mix</h4>
            <p>A mega mix of 75 favorites from the last...</p>
          </div>

          <div class="item">
            <img src="https://i.scdn.co/image/ab67616d0000b2733b5e11ca1b063583df9492db" />
            <div class="play">
              <span class="fa fa-play"></span>
            </div>
            <h4>All out 80s</h4>
            <p>The biggest songs of the 1090s.</p>
          </div>
        </div>
      </div>

      <div class="spotify-playlists">
        <h2>Focus</h2>
        <div class="list">
          <div class="item">
            <img src="https://i.scdn.co/image/ab67616d0000b2733b5e11ca1b063583df9492db" />
            <div class="play">
              <span class="fa fa-play"></span>
            </div>
            <h4>Peaceful Piano</h4>
            <p>Relax and indulge with beautiful piano pieces</p>
          </div>

          <div class="item">
            <img src="https://i.scdn.co/image/ab67616d0000b2733b5e11ca1b063583df9492db" />
            <div class="play">
              <span class="fa fa-play"></span>
            </div>
            <h4>Deep Focus</h4>
            <p>Keep calm and focus with ambient and pos...</p>
          </div>

          <div class="item">
            <img src="https://i.scdn.co/image/ab67616d0000b2733b5e11ca1b063583df9492db" />
            <div class="play">
              <span class="fa fa-play"></span>
            </div>
            <h4>Instrumental Study</h4>
            <p>Focus with soft study music in the...</p>
          </div>

          <div class="item">
            <img src="https://i.scdn.co/image/ab67616d0000b2733b5e11ca1b063583df9492db" />
            <div class="play">
              <span class="fa fa-play"></span>
            </div>
            <h4>chill lofi study beats</h4>
            <p>The perfect study beats, twenty four...</p>
          </div>

          <div class="item">
            <img src="https://i.scdn.co/image/ab67616d0000b2733b5e11ca1b063583df9492db" />
            <div class="play">
              <span class="fa fa-play"></span>
            </div>
            <h4>Coding Mode</h4>
            <p>Dedicated to all the programmers out there.</p>
          </div>
          
          <div class="item">
            <img src="https://i.scdn.co/image/ab67616d0000b2733b5e11ca1b063583df9492db" />
            <div class="play">
              <span class="fa fa-play"></span>
            </div>
            <h4>Focus Flow</h4>
            <p>Uptempo instrumental hip hop beats.</p>
          </div>

          <div class="item">
            <img src="https://i.scdn.co/image/ab67616d0000b2733b5e11ca1b063583df9492db" />
            <div class="play">
              <span class="fa fa-play"></span>
            </div>
            <h4>Calm Before The Storm</h4>
            <p>Calm before the storm music.</p>
          </div>

          <div class="item">
            <img src="https://i.scdn.co/image/ab67616d0000b2733b5e11ca1b063583df9492db" />
            <div class="play">
              <span class="fa fa-play"></span>
            </div>
            <h4>Beats to think to</h4>
            <p>Focus with deep techno and tech house.</p>
          </div>
        </div>
      </div>

      <div class="spotify-playlists">
        <h2>Mood</h2>
        <div class="list">
          <div class="item">
            <img src="https://i.scdn.co/image/ab67616d0000b2733b5e11ca1b063583df9492db" />
            <div class="play">
              <span class="fa fa-play"></span>
            </div>
            <h4>Mood Booster</h4>
            <p>Get happy with today's dose of feel-good...</p>
          </div>

          <div class="item">
            <img src="https://i.scdn.co/image/ab67616d0000b2733b5e11ca1b063583df9492db" />
            <div class="play">
              <span class="fa fa-play"></span>
            </div>
            <h4>Feelin' Good</h4>
            <p>Feel good with this positively timeless...</p>
          </div>

          <div class="item">
            <img src="https://i.scdn.co/image/ab67616d0000b2733b5e11ca1b063583df9492db" />
            <div class="play">
              <span class="fa fa-play"></span>
            </div>
            <h4>Dark & Stormy</h4>
            <p>Beautifully dark, dramatic tracks.</p>
          </div>

          <div class="item">
            <img src="https://i.scdn.co/image/ab67616d0000b2733b5e11ca1b063583df9492db" />
            <div class="play">
              <span class="fa fa-play"></span>
            </div>
            <h4>Feel Good Piano</h4>
            <p>Happy vibes for an upbeat morning.</p>
          </div>

          <div class="item">
            <img src="https://i.scdn.co/image/ab67616d0000b2733b5e11ca1b063583df9492db" />
            <div class="play">
              <span class="fa fa-play"></span>
            </div>
            <h4>Feelin' Myself</h4>
            <p>The hip-hop playlist that's a whole mood...</p>
          </div>

          <div class="item">
            <img src="https://i.scdn.co/image/ab67616d0000b2733b5e11ca1b063583df9492db" />
            <div class="play">
              <span class="fa fa-play"></span>
            </div>
            <h4>Chill Tracks</h4>
            <p>Softer kinda dance</p>
          </div>

          <div class="item">
            <img src="https://i.scdn.co/image/ab67616d0000b2733b5e11ca1b063583df9492db" />
            <div class="play">
              <span class="fa fa-play"></span>
            </div>
            <h4>Feel-Good Indie Rock</h4>
            <p>The best indie rock vibes - classic and...</p>
          </div>

          <div class="item">
            <img src="https://i.scdn.co/image/ab67616d0000b2733b5e11ca1b063583df9492db" />
            <div class="play">
              <span class="fa fa-play"></span>
            </div>
            <h4>idk.</h4>
            <p>idk.</p>
          </div>
        </div>

        <hr>
      </div>

      </div>
      <div class="player">
        <div class="volume-controls"></div>
        <div class="player-center">
          <div class="player-controls">
            <img
              style="height: 1rem; margin-right: 1rem; opacity: 0.8"
              src="./assets/shuffle.png"
            />
            <img
              style="height: 1rem; margin-right: 1rem; opacity: 0.8"
              src="./assets/previous_musicbar.png"
            />
            <img
              style="height: 2rem; margin-right: 1rem"
              src="./assets/play_musicbar.png"
            />
            <img
              style="height: 1rem; margin-right: 1rem; opacity: 0.8"
              src="./assets/next_musicbar.png"
            />
            <img
              style="height: 1rem; margin-right: 1rem; opacity: 0.8"
              src="./assets/loop.png"
            />
          </div>
          <div class="player-timeline">
            <span class="current-time">0:00</span>
            <input
              type="range"
              class="timeline-slider"
              min="0"
              max="100"
              step="1"
              value="0"
            />
            <span class="total-time">0:00</span>
          </div>
        </div>

        <div class="volume-controls">
          <img
            src="./assets/device.png"
            style="height: 1rem; margin-right: 1rem; opacity: 0.8"
            alt="Volume"
          />
          <img
            src="./assets/playlist.png"
            style="height: 1rem; margin-right: 1rem; opacity: 0.8"
            alt="Settings"
          />
          <img
            src="./assets/music.png"
            style="height: 1rem; margin-right: 1rem; opacity: 0.8"
            alt="Fullscreen"
          />
          <input
            type="range"
            class="volume-slider"
            min="0"
            max="100"
            step="1"
            value="50"
          />
        </div>
      </div>
    </div>
       <script
      src="https://kit.fontawesome.com/23cecef777.js"
      crossorigin="anonymous"
    ></script>
  </body>
</html>
